# README #

To change this README, you must have permission to change the master branch. You can see who has permission at: https://bitbucket.org/ooiea/ea_camds/src/production. Alternatively, you can create a branch, edit the README in the branch, and create a Pull Request to get it merged back into the master branch.

This captures an overview of the EA CAMDS Control Software as well as the steps needed to prep for deployment of the EA CAMDS Control Software.

### Contribution guidelines ###
* Follow the flow shown in the [BitBucket User's Guide](https://bitbucket.org/snippets/ooicgsn/7K6dM/bitbucket-git-users-guide). If you are new to Git or version control, the [BitBucket Overview](https://bitbucket.org/snippets/ooicgsn/yjgzd/bitbucket-git-overview-pdf) provides a simple overview to help understand the flow that is described in more detail in the aforementioned [User's Guide](https://bitbucket.org/snippets/ooicgsn/7K6dM/bitbucket-git-users-guide). 
    * Create a new branch for any new development
    * Ensure any updates are properly reviewed/tested prior to deployment by creating pull requests in BitBucket
* Writing tests - None
* Other guidelines - None

Who do I talk to?
Contact Chris Holm christopher.holm@oregonstate.edu or Christopher Wingard


* Version
This project uses [Semantic Versioning](https://semver.org/) with Major:Minor:Patch levels designtated in the VERSION
file. Be sure to include/update the version level as appropriate based on the guidlines for Semantic Versioning and
provide that information in the pull request so the project admin can appropriately tag the code for automated builds
and testing.

